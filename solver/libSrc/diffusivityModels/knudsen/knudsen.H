/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::diffusivityModels::knudsen

Description
    A knudsen diffusivity model

    //  D_{knudsen} = (poreDiameter/2)*97*sqrt(T/MW)  [m^2/s]
    //  where
    //      poreDiameter = (m)
    //      T ............ (K)
    //      MW ........... (kg/kmol) ... but reads in kg/mol!
    //  Geankoplis, Christie J, Transport Processes and Unit Operations,
    //  second edition (1983), Allyn and Bacon Series in Engineering,
    //  ISBN 0-205-07788-9, page 452.

    The input dictionary, diffusivity, is of the following form:

    \verbatim
    diffusivity
    {
        type     knudsen;
        Tname    T;                              // T (temperature) file name
        dPore    dPore [0 3 0 0 0 0 0] 8e-06;    // density
        MW       MW [1 0 0 0 -1 0 0] 31.9988e-3; // O2 (kg/mol)
    }
    \endverbatim

SourceFiles
    knudsen.C

\*---------------------------------------------------------------------------*/

#ifndef knudsen_H
#define knudsen_H

#include "diffusivityModel.H"
#include "dimensionedScalar.H"
#include "primitiveFieldsFwd.H"
#include "labelList.H"
#include "volFieldsFwd.H"
#include "fvMesh.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace diffusivityModels
{

/*---------------------------------------------------------------------------*\
                           Class knudsen Declaration
\*---------------------------------------------------------------------------*/

class knudsen
:
    public diffusivityModel
{


protected:

    // Protected data

        //- Name of temperature field
        word Tname_;

        //- The pore diameter
        const dimensionedScalar dPore_;

        //- The molecular weight
        const dimensionedScalar MW_;


public:

    //- Runtime type information
    TypeName("knudsen");


    // Constructors

        //- Construct from mesh, diffsivity field labelList and dictionary
        knudsen
        (
            const fvMesh& mesh,
            scalarField& diff,
            const labelList& cells,
            const dictionary& dict
        );

        //- Construct from components
        knudsen
        (
            const fvMesh& mesh,
            scalarField& diff,
            const labelList& cells,
            word Tname,
            const dimensionedScalar& dPore,
            const dimensionedScalar& MW
        );


    // Destructor

        ~knudsen()
        {}


    // Member functions

        //- Return the molecular weight
        const dimensionedScalar MW() const
        {
            return MW_;
        }

        //- Return the pore diameter
        const dimensionedScalar dPore() const
        {
            return dPore_;
        }

        //- Provide feedback for user
        void writeData();

        //- Evaluate the diffusivity
        void evaluate();

        //- Model info
        bool isFixed()
        {
            return 0;
        }
        bool isBinary()
        {
            return 0;
        }
        bool isKnudsen()
        {
            return 1;
        }

        void setSpecies(word spA, word spB)
        {
            // not implemented
        }

};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace diffusivityModels
} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
