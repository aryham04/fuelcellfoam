/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::mappingModel

Description
    Collects the parent-child mapping for all child meshes.

\*---------------------------------------------------------------------------*/

#ifndef mappingModel_H
#define mappingModel_H

#include "fvMesh.H"
#include "mapMesh.H"
#include "patchDatabase.H"
#include "checkFieldLimits.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class mappingModel Declaration
\*---------------------------------------------------------------------------*/

class mappingModel
:
    private checkFieldLimits
{
private:

    // Private data

        //- Reference to global mesh
        const fvMesh& mesh_;

        //- Mapping between air and global mesh
        mapMesh airMesh_;
        //- Mapping between fuel and global mesh
        mapMesh fuelMesh_;
        //- Mapping between electrolyte and global mesh
        mapMesh electrolyteMesh_;
        //- Mapping between interconnect and global mesh
        mapMesh interconnectMesh_;
        //- Database for patch names and IDs
        patchDatabase& pm_;
        //- Addressing of electrolyte anode to global mesh
        labelList electrolyteAnodeMap_;
        //- Addresssing of electrolyte cathode to global mesh
        labelList electrolyteCathodeMap_;

    // Private member functions

        //- Disallow copy construct
        mappingModel(const mappingModel&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const mappingModel&) = delete;


public:

    // Constructors

        mappingModel
	(
	    const fvMesh&, 
	    const fvMesh&, 
	    const fvMesh&, 
	    const fvMesh&, 
	    const fvMesh&, 
	    patchDatabase&
	);

    // Destructor

        ~mappingModel(){};


    // Member Functions

   	//- Map field from global to all child meshes
        void mapFromCell
	(
	    const volScalarField&, 
	    volScalarField&, 
	    volScalarField&, 
	    volScalarField&, 
	    volScalarField&
	);

	//- Return reference to air-global mesh mapping
    	mapMesh& air()
    	{
    	    return airMesh_;
    	}

        //- Return reference to fuel-global mesh mapping
        mapMesh& fuel()
        {
            return fuelMesh_;
        }

        //- Return reference to electrolyte-global mesh mapping
        mapMesh& electrolyte()
        {
            return electrolyteMesh_;
        }

        //- Return reference to interconnect-global mesh mapping
        mapMesh& interconnect()
        {
	    return interconnectMesh_;
        }

	//- Return reference to electrolyte-anode to global mesh mapping
        labelList& electrolyteAnodeMap()
        {
           return electrolyteAnodeMap_;
        }

	//- Return reference to electrolyte-cathode to global mesh mapping
        labelList& electrolyteCathodeMap()
        {
            return electrolyteCathodeMap_;
        }

	//- Set internal field to zero
        void clear(volScalarField&);
        //- Set internal field to zero
        void clear(surfaceScalarField&);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
