/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

\*---------------------------------------------------------------------------*/

#include "butlerVolmer.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "testFunction.H"
#include "RiddersRoot.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(butlerVolmer, 0);
    addToRunTimeSelectionTable(activationOverpotentialModel, butlerVolmer, dictionary);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::butlerVolmer::butlerVolmer
(
    const speciesTable& speciesNames,
    const dictionary& dict
)
:
    activationOverpotentialModel(speciesNames, dict)
{}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

const Foam::tmp<Foam::scalarField> Foam::butlerVolmer::exchangeCurrentDensity
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction
) const
{
    Foam::tmp<scalarField> pexchangeCurrentDensity
    (
        new scalarField (electrodeT.size(), 1)
    );
    scalarField& exCurrentDensity = pexchangeCurrentDensity.ref();

    // Compute local exchange current density
    forAll(stoichiometricNumbers(), fsi)
    {
        if(activeSwitch()[fsi])
        {
            exCurrentDensity *= Foam::pow
            (
                (pPatch*molFraction[fsi]/physicalConstant::pAtm.value()),
                stoichiometricNumbers()[fsi]
            );
        }
    }

    exCurrentDensity *= gamma().value();
    exCurrentDensity *= Foam::exp
    (
        E().value()/(physicalConstant::Rgas.value()*electrodeT)*(electrodeT/Tref().value() - 1.)
    );

    return pexchangeCurrentDensity;
}


Foam::scalar Foam::butlerVolmer::localCurrentDensity
(
    scalar i0,
    scalar T,
    scalar eta
) const
{
    scalar currentDensity;

    scalar A = 2*alpha()*physicalConstant::F.value()/(physicalConstant::Rgas.value()*T);
    scalar B = -2*(1-alpha())*physicalConstant::F.value()/(physicalConstant::Rgas.value()*T);

    currentDensity = Foam::exp
    (
        A*eta
    );
    currentDensity -= Foam::exp
    (
        B*eta
    );

    currentDensity *= i0;

    return currentDensity;
}


Foam::tmp<Foam::scalarField> Foam::butlerVolmer::currentDensity
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction,
    const scalarField& overPotential
) const
{
    Foam::tmp<scalarField> pcurrentDensity
    (
        new scalarField (electrodeT.size(), 1)
    );

    scalarField& cCurrentDensity = pcurrentDensity.ref();

    scalarField i0 = exchangeCurrentDensity(electrodeT, pPatch, molFraction).ref();

    forAll(electrodeT, cellI)
    {
        cCurrentDensity[cellI] = localCurrentDensity(i0[cellI], electrodeT[cellI], overPotential[cellI]);
    }

    return pcurrentDensity;
}


Foam::tmp<Foam::scalarField> Foam::butlerVolmer::overPotential
(
    const scalarField& electrodeT,
    const scalarField& pPatch,
    const PtrList<scalarField> molFraction,
    const scalarField& currentDensity
) const
{
    tmp<scalarField> pOverPotential
    (
        new scalarField(electrodeT.size(), 0.0)
    );

    scalarField& cOverPotential = pOverPotential.ref();

    scalarField i0 = exchangeCurrentDensity(electrodeT, pPatch, molFraction).ref();

    forAll(electrodeT, cellI)
    {
       testFunction tf(currentDensity[cellI], electrodeT[cellI], i0[cellI], *this);

       cOverPotential[cellI] = RiddersRoot<testFunction>(tf, 1e-5).root(0., 1.2);  
    }

    return pOverPotential;
}

// ************************************************************************* //
