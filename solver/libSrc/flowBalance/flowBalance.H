/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright held by original author
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Class
    Foam::flowBalance

Description
    Computes Reynolds numbers and gas/species fluxes at different patches
    to check continuity.

\*---------------------------------------------------------------------------*/

#ifndef flowBalance_H
#define flowBalance_H

#include "IOdictionary.H"
#include "fvMesh.H"
#include "materialDatabase.H"
#include "patchDatabase.H"
#include "liquidWaterModel.H"
#include "phaseChange.H"

namespace Foam
{

/*---------------------------------------------------------------------------*\
                    Class flowBalance Declaration
\*---------------------------------------------------------------------------*/

class flowBalance 
:
    public IOdictionary
{

private:

    // Private data

	//- Hydraulic diameter for Reynolds number calculation
	const dimensionedScalar dHyd_;
	//- Calc Reynolds number or not
	bool Re_;

	const fvMesh& mesh_;
	const volVectorField& U_;
	//- Source term vapour-liquid (i.e. phase change)
	const volScalarField& Svl_; 
	//- Density of liquid water
	const dimensionedScalar rhoW_;

	//- Database for gas phase properties
	materialDatabase& mat_;
	//- Database for patch names and IDs
	patchDatabase& pm_;

        label inletID_;
        label outletID_;
	//- Reacting patch (anode, cathode)
        label reactID_;

	//- Air or fuel phase
	bool isAir_;

    // Private member functions

        //- Disallow copy construct
        flowBalance(const flowBalance&) = delete;

        //- Disallow default bitwise assignment
        void operator=(const flowBalance&) = delete;
	
public:

    // Constructors

        flowBalance(materialDatabase&, patchDatabase&, liquidWaterModel&, phaseChange&);

    // Destructor

        ~flowBalance(){};


    // Member Functions

	//- Calculate Reynolds number
	void calcRe();

	//- Compute fluxes at patches
	void calcFluxes(List<scalarField>&);
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
